using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Database
{
    public class DbEntities : DbContext
    {
        public DbEntities(DbContextOptions<DbEntities> options) : base(options)
        {
        }

        protected DbEntities()
        {
        }
        public DbSet<Osoba> People { get; set; }
    }
}
