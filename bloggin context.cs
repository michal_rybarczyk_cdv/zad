
using DB.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB
{
    public class BloggingContextFactory : IDesignTimeDbContextFactory<DbEntities>
    {
        public DbEntities CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DbEntities>();
            optionsBuilder.UseSqlServer(@"Data Source=LAPTOP-U4EQMLEC\ZADANIE; Initial Catalog=bazadanych;User ID=sa; Password=obiektowka");

            return new DbEntities(optionsBuilder.Options);
        }
    }
}
