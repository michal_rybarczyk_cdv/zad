using DB.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;

namespace DB
{
    class Program
    {
        static void Main(string[] args)
        {

            string connectionString = @"Data Source=LAPTOP-U4EQMLEC\ZADANIE; Initial Catalog=bazadanych;User ID=sa; Password=obiektowka";
            var services = new ServiceCollection();
            services.AddDbContext<DbEntities>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            var provider = services.BuildServiceProvider();
            var db = provider.GetService<DbEntities>();

            Console.WriteLine("Imie");
            var imie = Console.ReadLine();
            Console.WriteLine("Nazwisko");
            var nazwisko = Console.ReadLine();
            var osoba = new Osoba
            {
                Imie = imie,
                Nazwisko = nazwisko            
            };
            

            db.People.Add(osoba);
            db.SaveChanges();

            var people = db.People.ToList();

            foreach (var item in people)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadKey();

        }
    }
}
