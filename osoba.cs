using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Database

{
    public class Osoba 
    {
        public int OsobaID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        

        public override string ToString()
        {
            return $"Imie : {Imie}, Nazwisko : {Nazwisko}";
        }
    }
}
